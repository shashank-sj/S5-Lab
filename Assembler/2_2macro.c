#include <stdio.h>
#include <string.h>
#include <stdlib.h>
struct inst
{
    char mne[20], opnd[20], la[20];
} in;
int main()
{
    FILE *fin, *fname, *fdef, *farg, *fout;
    int i, len;
    char name[20], arg[20];
    fin = fopen("inMP.txt", "r");
    fname = fopen("nametab.txt", "r");
    fdef = fopen("deftab.txt", "r");
    farg = fopen("argtab.txt", "w+");
    fout = fopen("opOut.txt", "w");
    fscanf(fin, "%s%s%s", in.la, in.mne, in.opnd);
    while (strcmp(in.mne, "END") != 0)
    {
        if (strcmp(in.mne, "MACRO") == 0)
        {
            fscanf(fin, "%s%s%s", in.la, in.mne, in.opnd);
            while (strcmp(in.mne, "MEND") != 0)
                fscanf(fin, "%s%s%s", in.la, in.mne, in.opnd);
        }
        else
        {
            fscanf(fname, "%s", name);
            if (strcmp(in.mne, name) == 0)
            {
                len = strlen(in.opnd);
                for (i = 0; i < len; i++)
                {
                    if (in.opnd[i] != ',')
                        fprintf(farg, "%c", in.opnd[i]);
                    else
                        fprintf(farg, "\n");
                }
                fseek(fname, SEEK_SET, 0);
                fseek(farg, SEEK_SET, 0);
                fscanf(fdef, "%s%s", in.mne, in.opnd);
                //fprintf(fout, ".\t%s\t%s\n", in.mne, in.opnd);
                fscanf(fdef, "%s%s", in.mne, in.opnd);
                while (strcmp(in.mne, "MEND") != 0)
                {
                    if ((in.opnd[0] == '&'))
                    {
                        fscanf(farg, "%s", arg);
                        fprintf(fout, "-\t%s\t%s\n", in.mne, arg);
                    }
                    else
                        fprintf(fout, "-\t%s\t%s\n", in.mne, in.opnd);
                    fscanf(fdef, "%s%s", in.mne, in.opnd);
                }
            }
            else
                fprintf(fout, "%s\t%s\t%s\n", in.la, in.mne, in.opnd);
        }
        fscanf(fin, "%s%s%s", in.la, in.mne, in.opnd);
    }
    fprintf(fout, "%s\t%s\t%s\n", in.la, in.mne, in.opnd);
    fclose(fin);
    fclose(fname);
    fclose(fdef);
    fclose(farg);
    fclose(fout);
    printf("PASS 2: Completed.\n");
    return 0;
}