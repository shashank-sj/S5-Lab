#include <stdio.h>
#include <string.h>
#define max 15
struct symb
{
    int hash;
    int add;
    char label[10];
} sy[max], a;
int count = -1;
FILE *fp;
void sethashes()
{
    for (int i = 0; i < max; i++)
        sy[i].hash = -1;
}
int createHash(int key)
{
    return key % max;
}
void display()
{
    fp = fopen("symbols.txt", "w+");
    for (int i = 0; i < max; i++)
    {
        printf("\n%d\t %d\t %s", i, sy[i].add, sy[i].label);
        fprintf(fp, "\n%d %d %s", i, sy[i].add, sy[i].label);
    }
    fclose(fp);
}
void insert()
{
    printf("\nEnter the address:");
    scanf("%d", &a.add);
    printf("enter The label:");
    scanf("%s", a.label);
    int hash = createHash(a.add);
    if (count < max)
    {
        if (sy[hash].hash == -1)
        {
            ;
        }
        else
        {
            int insertflag = 0;
            int t = hash;
            while (t < max && insertflag == 0)
            {
                if (sy[t].hash == -1)
                {
                    hash = t;
                    insertflag = 1;
                }
                else
                    t++;
            }
            t = 0;
            while (t < hash && insertflag == 0)
            {
                if (sy[t].hash == -1)
                {
                    hash = t;
                    insertflag = 1;
                }
                else
                    t++;
            }
        }
        sy[hash].hash = hash;
        sy[hash].add = a.add;
        strcpy(sy[hash].label, a.label);
    }
    else
        printf("\ntabel is full\n");
}
int search()
{
    int f=0;
    char find[max];
    printf("enter the label to search:");
    scanf("%s",find);
    for(int i=0;i<max;i++)
    {
        if(strcmp(sy[i].label,find)==0)
        {
            f=1;
            printf("\nThe label --%s-- is present in the symbol table at address:%d\n", sy[i].label,sy[i].add);
        }
    }
}
int main(int argc, char const *argv[])
{
    int i = 0;
    sethashes();
    char ans;
    do
    {
        printf("\nSymbol menu:\n\t1.Create new Symbol tabel\n\t2.Search for a Symbol\n\t3.View Symbol tabel\nEnter your choice:");
        scanf("%d", &i);
        switch (i)
        {
        case 1:
            do
            {
                insert();
                display();
                printf("\nContinue(y/n)?");
                scanf("%c", &ans);
                scanf("%c", &ans);

            } while (ans == 'y');
            display();
            break;
        case 2:
            if (search() == -1)
                printf("\nlabel not in Symbol tabel :(\n");
            break;
        case 3:
            display();
            break;
        case 4:
            return 1;
        }
    } while (1);
    return 0;
}